import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class MyWebAPIService {
  
  constructor(private jsonp: Jsonp) {}
  
  getList() {
    let apiUrl = 'http://jamfco.com/api.php';
    
    let params = new URLSearchParams();
    params.set('action', 'fetch');
    params.set('ref', 'survey');
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');

    // TODO: Add error handling
    
    return this.jsonp
            .get(apiUrl, { search: params })
            .map(response => <string[]> response.json());

  }

  putResponse( key: string, body:string ) {
    let apiUrl = 'http://jamfco.com/api.php';
    
    let params = new URLSearchParams();
    params.set('action', 'push');
    params.set('ref', 'survey');
    params.set('fkey', key);
    params.set('body', body);
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');

    // TODO: Add error handling
    return this.jsonp
                  .get(apiUrl, { search: params })
                  .map(response => <string[]> response.json());
  }
  

}