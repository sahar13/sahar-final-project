import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MyWebAPIService } from 'src/survey/mywebapi.service';
import { Survey } from './survey';

@Component({
  selector: 'app-survey',
  templateUrl: 'src/survey/survey.component.html'
  providers: [ MyWebAPIService ]
})

export class SurveyComponent implements OnInit {

  items: Observable<string[]>;
  update: string[];

  ref: string = "apple";
  key: string = "survey";
    
  constructor (private mywebapiservice: MyWebAPIService) { }

  ngOnInit() {
  
  }
  ages = ['18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30+'];

  model = new Survey('name', this.ages[0], 'city', 'course', 'outcome', 'learning', 'dreamjob', 'company');

  submitted = false;
  
  

  onSubmit() {
      let body = { name: this.name, age: this.age, city: this.city, course: this.course, course: this.course, outcome: this.outcome, learning: this.learning, dreamjob: this.dreamjob, company: this.company};
    
      this.mywebapiservice
          .putResponse( this.key, JSON.stringify( body ) )
          .subscribe( 
            message => this.update = message, 
            error => console.log("Error: ", error),
            () => this.items = this.mywebapiservice.getList()
          );
    }




}
