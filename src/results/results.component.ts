import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MyWebAPIService } from 'src/survey/mywebapi.service';

@Component({
  selector: 'app-results',
  templateUrl: 'src/results/results.component.html',
  styleUrl: 'src/results/results.component.css',
  providers: [ MyWebAPIService ] 
})
export class ResultsComponent implements OnInit {
  
  items: Observable<string[]>;
	
	constructor (private mywebapiservice: MyWebAPIService) { }
	
	ngOnInit(): void {
		// Load in the values from the service.
	    this.items = this.mywebapiservice.getList();
	}

	convert(value:string):string {
		try {
	      let p = JSON.parse(value);
	      return "name: " + p.name + " age: " + p.age + " city: " + p.city + " course: " + p.course + " outcome: " + p.outcome + " learning: " + p.learning + " dreamjob: " + p.dreamjob + " company: " + p.company;
	    }
	    catch(e) {
	      return value;
	    } 
	}

}
