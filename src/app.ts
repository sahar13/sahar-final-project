//our root app component
import {Component, NgModule, VERSION} from '@angular/core'
import { NgModule }      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { RouterModule, Routes } from '@angular/router';
import { ResultsComponent } from './results/results.component';
import { SurveyComponent } from './survey/survey.component'

@Component({
  selector: 'my-app',
  template: `
    <div>
      <h2>Introduce Yourself!</h2>
    </div>
    
    <div class="container">
    <nav>
        <a class='btn btn-default' routerLink="/survey">Survey</a>
        <a class='btn btn-default' routerLink="/results">Results</a>
    </nav>
    <router-outlet></router-outlet>

    </div>

  `,
})
export class App {
  name:string;
  constructor() {
    this.name = `Angular! v${VERSION.full}`
  }
}

const appRoutes: Routes = [
      { path: 'results', component: ResultsComponent },
      { path: 'survey', component: SurveyComponent },
      { path: '',
          redirectTo: '/survey',
          pathMatch: 'full'
        }

    ];


@NgModule({
  imports: [ 
  BrowserModule,
  FormsModule,
  HttpModule,
  JsonpModule,
  RouterModule.forRoot(appRoutes)
  ],
  declarations: [ 
  App,
  ResultsComponent,
  SurveyComponent
  ],
  bootstrap: [ App ]
})
export class AppModule {}